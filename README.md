# New Project Template

This is a basic template for creating new projects

## Project Structure

```
+-- README.md
+-- src // source code
+-- .gitattributes
+-- .gitignore
+-- .dockerignore // ignore files for docker building process
+-- sonar-project.properties //SonarQube configuration
+-- .gitlab-ci.yml // CI/CD pipeline
+-- Dockerfile  // each project has its own Dockerfile
+-- parse.sh  // parse script
+-- deploy.sh  // deployment script
```
## Quick start
Please note that you **DON'T** need to update any variables start with `$`. Please follow these steps below for the best result.
- make sure you have root access to kubernetes master node
- ssh to all your kubernetes nodes as root and run the following comand. Please replace `{{DOCKER_REGISTRY_PLACEHOLDER}}` with the your docker registry URL. The correct URL should look like this `1.1.1.1:5000`
```
      echo "{\"insecure-registries\" : [\"{{DOCKER_REGISTRY_PLACEHOLDER}}\"] }" > /etc/docker/daemon.json && systemctl restart docker
```
- Replace `{{DOCKER_REGISTRY_PLACEHOLDER}}` in `.gitlab-ci.yml`. Please remove `http` or `https` in the url. The correct URL should look like this `1.1.1.1:5000`
- Replace `{{CONTAINER_PORT_PLACEHOLDER}}` in `deployment.yaml`. This is the port your application is listening to
- Replace `{{CONTAINER_PORT_PLACEHOLDER}}` and `{{EXTERNAL_PORT_PLACEHOLDER}}` in `service.yaml`
- Build your `Dockerfile`
- Configurate `sonar-project.properties` by following https://docs.sonarqube.org/display/SONAR/Analysis+Parameters
- push to master

## Deployment
After the pipeline finish you can access your service by using the following pattern:
`<minion_ip>:<nodePort>` // nodePort can be found in the `service.yaml`

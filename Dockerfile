# Node image
FROM node:10.8

# set working directory
RUN mkdir /usr/src/app
WORKDIR /usr/src/app

# copy source code
COPY . /usr/src/app/

# install and cache app dependencies
RUN npm install

# install sonar runner
RUN apt-get update -y && apt-get install -y wget unzip
RUN wget -N https://bintray.com/sonarsource/SonarQube/download_file?file_path=org%2Fsonarsource%2Fscanner%2Fcli%2Fsonar-scanner-cli%2F3.2.0.1227%2Fsonar-scanner-cli-3.2.0.1227-linux.zip > /dev/null 2>&1
RUN mv *.zip sonar-scanner-cli-3.2.0.1227-linux.zip
RUN unzip -o sonar-scanner-cli-3.2.0.1227-linux.zip > /dev/null 2>&1

# run sonarqube
RUN ./sonar-scanner-3.2.0.1227-linux/bin/sonar-scanner 2>&1 | tee sonar.txt && if grep -qF "FAILURE" sonar.txt;then exit 1; fi

RUN rm -rf sonar*

EXPOSE 3000

CMD [ "npm", "run", "prod" ]

# $1 PROJECT_NAME
if kubectl get deployment | grep $1
then
	kubectl delete -n default deployment $1
fi
kubectl create -f deployment.yaml

if kubectl get svc | grep $1
then
	kubectl delete svc $1
fi
kubectl create -f service.yaml
